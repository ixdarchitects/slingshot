﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Security;
using System.Security.AccessControl;

using Microsoft.Win32;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProvingGround.Slingshot.Installer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Target Paths
        private string _targetRevit2014AddinPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Autodesk", "Revit", "Addins", "2015"); //Revit Addins Folder
        private string _targetRevit2015AddinPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Autodesk", "Revit", "Addins", "2016"); //Revit Addins Folder
        private string _targetGrasshopperPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Grasshopper", "Libraries"); //Grasshopper Addons Folder

        // Source path
        private string _sourcepath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        /// <summary>
        /// Constructor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            Label_Version.Content = "Slingshot! v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        /// <summary>
        /// Install new files
        /// </summary>
        private void InstallFiles()
        {
            try
            {
                CleanUpOldFiles();
                DirectoryInfo m_dirSource = new DirectoryInfo(_sourcepath);

                FileInfo[] m_files = m_dirSource.GetFiles("*.*");

                if (System.IO.Directory.Exists(System.IO.Path.Combine(_targetGrasshopperPath, "Slingshot")) == false)
                {
                    System.IO.Directory.CreateDirectory(System.IO.Path.Combine(_targetGrasshopperPath, "Slingshot"));
                    System.IO.Directory.CreateDirectory(System.IO.Path.Combine(_targetGrasshopperPath, "Slingshot", "x64"));
                }

                // iterate through files
                foreach (FileInfo x in m_files)
                {
                    if (x.Name.EndsWith("dll"))
                    {
                        //install code here
                        if (x.Name == "ProvingGround.Slingshot.dll")
                        {
                            x.CopyTo(System.IO.Path.Combine(_targetGrasshopperPath, "Slingshot", "ProvingGround.Slingshot.gha"), true);
                        }
                        else if (x.Name == "SQLite.Interop.dll")
                        {
                            x.CopyTo(System.IO.Path.Combine(_targetGrasshopperPath, "Slingshot", "x64", x.Name), true);
                        }
                        else
                        {
                            x.CopyTo(System.IO.Path.Combine(_targetGrasshopperPath, "Slingshot", x.Name), true);
                        }
                    }
                }
            }
            catch (Exception ex) { System.Windows.Forms.MessageBox.Show(ex.ToString(), "Installation Failed", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        /// <summary>
        /// Remove old, out of date files from previous versions
        /// </summary>
        private void CleanUpOldFiles()
        {
            //add fill paths here
            List<string> m_oldfiles = new List<string>();

            m_oldfiles.Add(System.IO.Path.Combine(_targetGrasshopperPath, "SlingshotDB.gha"));
            //m_oldfiles.Add(System.IO.Path.Combine(_targetGrasshopperPath, "System.Data.SQLite.dll"));
            //m_oldfiles.Add(System.IO.Path.Combine(_targetGrasshopperPath, "MySql.Data.dll"));
            //m_oldfiles.Add(System.IO.Path.Combine(_targetGrasshopperPath, "SQLite.Interop.dll"));

            // delete files
            foreach (string f in m_oldfiles)
            {
                if (System.IO.File.Exists(f))
                {
                    System.IO.File.Delete(f);
                }
            }
        }

        /// <summary>
        /// Close Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Install Tools
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Install_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (System.IO.Directory.Exists(_targetGrasshopperPath))
                {
                    // install the files
                    InstallFiles();
                    System.Windows.Forms.MessageBox.Show("Slingshot has been installed!", "Install Successful!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("We could not find Grasshopper on your machine.  Please make sure you have Rhino 5.0 and the latest version of Grasshopper before proceeding.", "Grasshopper", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex){ System.Windows.Forms.MessageBox.Show(ex.ToString(), "Installation Failed", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        /// <summary>
        /// Drag Window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}
